package com.pack.lazy;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

@Configuration
@Lazy
public class BeanConfig {
     @Bean
     public EmployeeBean1 empBean1() {
    	 return new EmployeeBean1();
     }
     @Bean
     public EmployeeBean2 empBean2() {
    	 return new EmployeeBean2();
     }
}
