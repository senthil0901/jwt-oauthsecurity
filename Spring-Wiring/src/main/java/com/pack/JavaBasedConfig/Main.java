package com.pack.JavaBasedConfig;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {

	public static void main(String[] args) {
		ApplicationContext ctx =
				new AnnotationConfigApplicationContext(ExampleConfig.class);
		        Example1 example1=ctx.getBean(Example1.class);
		        example1.setMessage1("Hello to Spring");
			    System.out.println(example1);
			    
			    Example1 example3=ctx.getBean(Example1.class);
			    System.out.println(example3);
			    
			    Example2 example2=ctx.getBean(Example2.class);
		        example2.setMessage2("Welcome to Spring");
			    System.out.println(example2);
			    
			    Example2 example4=ctx.getBean(Example2.class);
			    System.out.println(example4);
	}

}
