package com.pack.beanprocessor;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

	public static void main(String[] args) {
		AbstractApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");
		BookBean bookBean = (BookBean) context.getBean("bookBeanPost");
	     System.out.println(bookBean.getBookName());
	     context.registerShutdownHook();
	}

}
