package com.pack.lazy;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;



public class Main {

	public static void main(String[] args) {
		//Beanfactory lazy loads only if u call getBean
		/*Resource resource = new ClassPathResource("hello.xml");
        BeanFactory bf = new XmlBeanFactory(resource);
        EmployeeBean1 bean1=(EmployeeBean1)bf.getBean("bean1");
        EmployeeBean2 bean2=(EmployeeBean2)bf.getBean("bean2");*/
		
		//Without getBean() itself it loads all bean
		//To lazy load use lazy-init=true
	   // ApplicationContext appContext = new ClassPathXmlApplicationContext("hello.xml");
		
		//Using @Lazy 
		ApplicationContext ctx =
				new AnnotationConfigApplicationContext(BeanConfig.class);
	}

}
