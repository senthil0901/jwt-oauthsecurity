package com.pack.inherit;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("hello.xml");
	      
	      Example1 objA = (Example1) context.getBean("example1");
	      objA.getMessage1();
	      objA.getMessage2();

	      Example2 objB = (Example2) context.getBean("example2");
	      objB.getMessage1();
	      objB.getMessage2();
	      objB.getMessage3();
	}

}
