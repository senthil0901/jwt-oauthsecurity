package com.pack.aware;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

	public static void main(String[] args) {
		AbstractApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");
		AwareBeanImpl awareBeanImpl = (AwareBeanImpl) context.getBean("awareBean");
	     context.registerShutdownHook();
	}

}
