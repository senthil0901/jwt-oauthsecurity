package com.pack.collection;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("beans1.xml");
	      Example e=(Example)context.getBean("collection");

	     System.out.println(e.getAddressList());
	      System.out.println(e.getAddressSet());
	      System.out.println(e.getAddressMap());
	      System.out.println(e.getAddressProp());
	}

}
