package com.pack.behaviorVerification;


import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.*;
import java.time.LocalDate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class BookServiceTest {
	
	@InjectMocks
	private BookService bookService;
	
//	@Mock
//	private BookRepository bookRepository;
	
	@Spy
	private BookRepository bookRepository;
	
	//1. Verify simple invocation on mock
	@Test
	public void testAddBook() {
		Book book = new Book(null, "Mockito In Action", 600, LocalDate.now());
		bookService.addBook(book);
		verify(bookRepository).save(book);
	}
	
	//2. Verify number of interactions with mock
	@Test
	public void testSaveBookWithBookRequestWithGreaterPrice() {
		BookRequest bookRequest = new BookRequest("Mockito In Action", 500, LocalDate.now());
		Book book = new Book(null, "Mockito In Action", 500, LocalDate.now());
		bookService.addBook(bookRequest);
		verify(bookRepository, times(0)).save(book);
	}
	//3. Verify number of interactions with mock and verify bookRepository called twice
	@Test
	public void testSaveBookWithBookRequestWithGreaterPrice1() {
		BookRequest bookRequest = new BookRequest("Mockito In Action", 600, LocalDate.now());
		Book book = new Book(null, "Mockito In Action", 600, LocalDate.now());
		bookService.addBook(bookRequest);
		bookService.addBook(bookRequest);
		verify(bookRepository, times(2)).save(book);
	}
	
	
	//4. Verify an interaction has not occured
	@Test
	public void testSaveBookWithBookRequestWithGreaterPrice2() {
		BookRequest bookRequest = new BookRequest("Mockito In Action", 500, LocalDate.now());
		Book book = new Book(null, "Mockito In Action", 500, LocalDate.now());
		bookService.addBook(bookRequest);
		verify(bookRepository, never()).save(book);
	}
	
	//5. Verify no interaction with Mock
	@Test
	public void testUpdatePrice() {
		bookService.updatePrice(null, 600);
		verifyNoInteractions(bookRepository);
	}
	
	//6. Verify no unexpected interaction
	@Test
	public void testUpdatePrice2() {
		Book book = new Book("1234", "Mockito In Action", 500, LocalDate.now());
		when(bookRepository.findBookById("1234")).thenReturn(book);  //stub the method
		bookService.updatePrice("1234", 500);
		verify(bookRepository).findBookById("1234");
		//verify(bookRepository).save(book);
		verifyNoMoreInteractions(bookRepository);
	}
	
	
	//mockito to call mock object in order, if we change the order test case failed
	//In BookService, first we call findBookById and then save
	@Test
	public void testUpdatePrice3() {
		Book book = new Book("1234", "Mockito In Action", 500, LocalDate.now());
		when(bookRepository.findBookById("1234")).thenReturn(book);
		bookService.updatePrice("1234", 500);
		
		InOrder inOrder = Mockito.inOrder(bookRepository);
		inOrder.verify(bookRepository).findBookById("1234");
		inOrder.verify(bookRepository).save(book);
	}
	
	@Test
	public void testSaveBookWithBookRequestWithGreaterPrice3() {
		BookRequest bookRequest = new BookRequest("Mockito In Action", 600, LocalDate.now());
		Book book = new Book(null, "Mockito In Action", 600, LocalDate.now());
		bookService.addBook(bookRequest);
		bookService.addBook(bookRequest);
		bookService.addBook(bookRequest);
	//verify(bookRepository, times(2)).save(book);
	verify(bookRepository, atLeast(4)).save(book);
	//	verify(bookRepository, atMost(2)).save(book);
//		verify(bookRepository, atMostOnce()).save(book);
	//	verify(bookRepository, atLeastOnce()).save(book);
	}
	
}