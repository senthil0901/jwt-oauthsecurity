package com.pack.anno;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main1 {

	public static void main(String[] args) {
		AnnotationConfigApplicationContext context = 
	            new AnnotationConfigApplicationContext(UserConfig.class);
	      
	      User user=context.getBean(User.class);
	      user.addUser();
	      
	      context.close();
	}

}
