package com.pack.scope;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class Main {

	public static void main(String[] args) {
		 ApplicationContext context = new ClassPathXmlApplicationContext("hello.xml");
		 Student stud=(Student)context.getBean(Student.class);
		 System.out.println(stud);
		 Student stud1=(Student)context.getBean(Student.class);
		 System.out.println(stud1);
		 Student stud2=(Student)context.getBean(Student.class);
		 System.out.println(stud2);
	}

}
