package com.pack.spies;

import static org.junit.Assert.assertEquals;

import static org.mockito.Mockito.doReturn;

import java.time.LocalDate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BookManagerTest {

	@InjectMocks
	private BookManager bookManager; //to test
	
	@Spy
	private BookService bookService; //to mock 
	//BookService bookService=Mockito.spy(BookService.class);
	
	
	@Test
	public void testMockitoSpy(){
		// We need to mock findBook because it is communicating to database or not implemented
		// We need to call getAppliedDiscount to calculate the discounted price
		Book book = new Book("1234", "Mockito In Action", 500, LocalDate.now());
		//doReturn(book).when(bookService).findBook("1234");
		
		when(bookService.findBook("1234")).thenReturn(book);
		int actualDiscount = bookManager.applyDiscountOnBook("1234", 10);
		assertEquals(450, actualDiscount);
	}
	
}